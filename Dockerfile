FROM centos

MAINTAINER JMiahMan

# install tools and update rpm to use ztsd for Fedora 31 rpm support
RUN dnf -y update ;\
    dnf -y install epel-release python-pip git make wget sudo vim rpmdevtools libzstd-devel python2-devel; \
    wget http://vault.centos.org/8.0.1905/BaseOS/Source/SPackages/rpm-4.14.2-11.el8_0.src.rpm; \
     rpm --version; \
    dnf builddep rpm-4.*; \
    rpmbuild --rebuild -ba --with zstd rpm-4.*.rpm; \
    rm -rf /root/rpmbuild/RPMS/x86_64/*debug*; \
    dnf -y install /root/rpmbuild/RPMS/x86_64/*.rpm /root/rpmbuild/RPMS/noarch/*.rpm; \
    yum clean all  

# install docker client
ARG DOCKERURL=https://download.docker.com/linux/static/stable/x86_64/docker-19.03.5.tgz
ARG DOCKERSHA256=50cdf38749642ec43d6ac50f4a3f1f7f6ac688e8d8b4e1c5b7be06e1a82f06e9
RUN set -eux &&\
    curl -fSL "$DOCKERURL" -o docker.tgz \
    && echo "$DOCKERSHA256 *docker.tgz" | sha256sum -c - \
    && tar -xzvf docker.tgz \
    && mv docker/* /usr/local/bin/ \
    && rmdir docker \
    && rm docker.tgz \
    && chmod +x /usr/local/bin/docker 
   
WORKDIR /tmp

# install entrypoint
COPY entrypoint.sh /usr/local/bin/ 
RUN set -eux &&\
    chmod +x /usr/local/bin/entrypoint.sh
   
ENTRYPOINT ["entrypoint.sh"]
