# centos8 docker-in-docker
Docker in Docker(dind) image based centos8
- base image: centos8
- docker19.03.5

# dind build and run

## build dind image
./build-centos8-dind

## run
docker run -d -v /var/run/docker.sock:/var/run/docker.sock centos8-dind  

# pull from docker hub
docker pull unitylinux/centos8-dind  
